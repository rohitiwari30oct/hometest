package com.hometesting.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hometesting.entity.Project;
import com.hometesting.repository.ProjectRepository;
import com.hometesting.service.ProjectService;
@Service
public class ProjectServiceImpl implements ProjectService{
	ProjectRepository projectRepository;
	@Autowired
	public ProjectServiceImpl(ProjectRepository projectRepository) {
		super();
		this.projectRepository = projectRepository;
	}
	

	@Override
	public Iterable<Project> findAllProject() {
		return projectRepository.findAll();
	}


	@Override
	public Project findProjectById(String project_id) {
		return projectRepository.findByProjectId(project_id);
	}


	@Override
	public Project addProject(Project project) {
		return projectRepository.save(project);
		
	}

}
