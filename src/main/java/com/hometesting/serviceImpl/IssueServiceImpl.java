package com.hometesting.serviceImpl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.UUID;

import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.sort.SortBuilders;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.data.elasticsearch.core.mapping.IndexCoordinates;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.Query;
import org.springframework.stereotype.Service;

import com.hometesting.config.Utility;
import com.hometesting.entity.Issue;
import com.hometesting.entity.Project;
import com.hometesting.entity.StateChangeLog;
import com.hometesting.repository.IssueRepository;
import com.hometesting.repository.IssueStateRepository;
import com.hometesting.repository.ProjectRepository;
import com.hometesting.repository.StateChangeLogRepository;
import com.hometesting.request.WeeklyIssueListRequest;
import com.hometesting.response.ProjectIssue;
import com.hometesting.response.WeeklyIssueListResponse;
import com.hometesting.response.model.IssueModel;
import com.hometesting.response.model.StateSummary;
import com.hometesting.response.model.WeeklySummary;
import com.hometesting.service.IssueService;

@Service
public class IssueServiceImpl implements IssueService {
	@Autowired
	IssueRepository issueRepository;
	@Autowired
	ProjectRepository projectRepository;
	@Autowired
	IssueStateRepository issueStateRepository;
	@Autowired
	StateChangeLogRepository stateChangeLogRepository;
	@Autowired
	ElasticsearchRestTemplate elasticsearchRestTemplate;

	@Override
	public Issue addNewIssue(Issue issue) {
		issue.setIssueId(UUID.randomUUID().toString());
		issue.setCreateDate(new Date());
		return issueRepository.save(issue);
	}

	@Override
	public Issue updateIssue(Issue issue) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Issue getIssueById(String issueId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Iterable<Issue> getIssuesByProjectId(String projectId) {
		return issueRepository.findAllByProject_ProjectId(projectId);
	}

	@Override
	public Iterable<Issue> getAllIssues() {
		return issueRepository.findAll();
	}

	@Override
	public WeeklyIssueListResponse getIssuesBetweenDates(WeeklyIssueListRequest request) {
		counter = 0;
		Date startDate = Utility.getStartDateOfWeek(request.getFrom_week());
		Date endDate = Utility.getEndDateOfWeek(request.getTo_week());
		Query searchQuery = new NativeSearchQueryBuilder().withFilter(
				QueryBuilders.boolQuery().must(QueryBuilders.rangeQuery("createDate").gt(startDate.getTime()))
						.must(QueryBuilders.rangeQuery("createDate").lt(endDate.getTime()))
						.must(QueryBuilders.multiMatchQuery(request.getTypes().toString(), "issue.issueType"))
						.must(QueryBuilders.multiMatchQuery(request.getStates().toString(), "issue.currentState.stateName"))
						.must(QueryBuilders.matchQuery("issue.project.projectId", request.getProject_id())
								.minimumShouldMatch("100%")))
				.withSort(SortBuilders.fieldSort("createDate")).build();
		SearchHits<StateChangeLog> searchedIssues = elasticsearchRestTemplate.search(searchQuery,StateChangeLog.class,
				IndexCoordinates.of("statechangelog"));
		WeeklyIssueListResponse response = new WeeklyIssueListResponse();
		response.setProject_id(request.getProject_id());
		createResponseFromSearchResult(response, searchedIssues);
		return response;
	}

	int counter = 0;
	String currentWeek = "";
	String lastWeek = "";
	Map<String, List<WeeklySummary>> weeklySummaries = new TreeMap<>();
	Map<String, List<IssueModel>> issueModels = new HashMap<>();

	// create response from searched result
	private void createResponseFromSearchResult(WeeklyIssueListResponse response, SearchHits<StateChangeLog> searchedIssues) {
		weeklySummaries = new TreeMap<>();
		issueModels = new HashMap<>();
		currentWeek = "";
		lastWeek = "";
		searchedIssues.forEach(sh -> {
			StateChangeLog stateChange = sh.getContent();
			Issue issue = stateChange.getIssue();

			// check if week changes, then add state summary to response and clean up issue
			// model list
			if (!currentWeek.equals(lastWeek)) {
				WeeklySummary weeklySummary = new WeeklySummary();
				weeklySummary.setWeek(lastWeek);
				List<StateSummary> stateSummarys = new ArrayList<>();

				// fetch all issues of last week and put in the state summary list
				for (Map.Entry<String, List<IssueModel>> entry : issueModels.entrySet()) {
					StateSummary stateSummary = new StateSummary();
					stateSummary.setCount(entry.getValue().size());
					stateSummary.setIssues(entry.getValue());
					stateSummary.setState(entry.getKey());
					stateSummarys.add(stateSummary);
				}
				weeklySummary.setState_summaries(stateSummarys);
				// end

				// update weekly summary list
				if (weeklySummaries.get(lastWeek) != null) {
					weeklySummaries.get(lastWeek).add(weeklySummary);
				} else {
					List<WeeklySummary> summaryList = new ArrayList<>();
					summaryList.add(weeklySummary);
					weeklySummaries.put(lastWeek, summaryList);
				}
				// end

				// initialize issue list for next week
				issueModels = new HashMap<>();
				lastWeek = currentWeek;
			}
			// end

			// creating issue filtered response and adding to list
			IssueModel issueModel = new IssueModel();
			issueModel.setIssue_id(issue.getIssueId());
			issueModel.setType(issue.getIssueType());
			if (issueModels.get(stateChange.getState().getStateName()) != null) {
				issueModels.get(stateChange.getState().getStateName()).add(issueModel);
			} else {
				List<IssueModel> issuemodels = new ArrayList<IssueModel>();
				issuemodels.add(issueModel);
				issueModels.put(stateChange.getState().getStateName(), issuemodels);
			}
			// end

			currentWeek = Utility.getWeekYearFromDate(stateChange.getCreateDate());
			if (lastWeek.equals("")) {
				lastWeek = currentWeek;
			}

		});

		WeeklySummary weeklySummary = new WeeklySummary();
		weeklySummary.setWeek(currentWeek);
		List<StateSummary> stateSummarys = new ArrayList<>();
		for (Map.Entry<String, List<IssueModel>> entry : issueModels.entrySet()) {
			StateSummary stateSummary = new StateSummary();
			stateSummary.setCount(entry.getValue().size());
			stateSummary.setIssues(entry.getValue());
			stateSummary.setState(entry.getKey());
			stateSummarys.add(stateSummary);
		}
		weeklySummary.setState_summaries(stateSummarys);
		if (weeklySummaries.get(currentWeek) != null) {
			weeklySummaries.get(currentWeek).add(weeklySummary);
		} else {
			List<WeeklySummary> summaryList = new ArrayList<>();
			summaryList.add(weeklySummary);
			weeklySummaries.put(currentWeek, summaryList);
		}

		for (Map.Entry<String, List<WeeklySummary>> entry : weeklySummaries.entrySet()) {
			response.getWeekly_summaries().addAll(entry.getValue());
		}
	}

	@Override
	public Issue findIssueById(String issueId) {
		return issueRepository.findByIssueId(issueId);
	}
	Issue issue = null;
	Project project = null;
	@Override
	public void saveIssuesFromThirdParty(ProjectIssue response) {
		project = projectRepository.findByProjectId(response.getProject_id());
		if (project == null) {
			project = new Project();
			project.setProjectId(response.getProject_id());
			project.setCreateDate(new Date());
			projectRepository.save(project);
		}
		response.getIssues().forEach(is -> {
			issue = issueRepository.findByIssueId(is.getIssue_id());
			if (issue == null) {
				issue = new Issue();
				issue.setIssueId(is.getIssue_id());
				issue.setCreateDate(new Date());
				issue.setIssueType(is.getType());
				issue.setCurrentState(issueStateRepository.findByStateName(is.getCurrent_state()));
				issue.setProject(project);
				issue = issueRepository.save(issue);
				is.getChangelogs().stream().forEach(chlog -> {
					StateChangeLog changeLog = stateChangeLogRepository.findByIssue_IssueIdAndState_stateName(is.getIssue_id(), chlog.getTo_state());
					if (changeLog == null) {
							changeLog = new StateChangeLog();
							changeLog.setId(UUID.randomUUID().toString());
							SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mma 'UTC'");
							try {
								changeLog.setCreateDate(format.parse(chlog.getChanged_on()));
							} catch (ParseException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							changeLog.setIssue(issue);
							changeLog.setState(issueStateRepository.findByStateName(chlog.getTo_state()));
							stateChangeLogRepository.save(changeLog);
						
					}
				});
			}
		});

	}

	@Override
	public Iterable<StateChangeLog> getAllIssueStateChnages() {
		return stateChangeLogRepository.findAll();
	}

}
