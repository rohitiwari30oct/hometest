package com.hometesting.request;

import java.util.List;

public class WeeklyIssueListRequest {
	String project_id;
	String from_week;
	String to_week;
	List<String> types;
	List<String> states;
	public String getProject_id() {
		return project_id;
	}
	public void setProject_id(String project_id) {
		this.project_id = project_id;
	}
	public String getFrom_week() {
		return from_week;
	}
	public void setFrom_week(String from_week) {
		this.from_week = from_week;
	}
	public String getTo_week() {
		return to_week;
	}
	public void setTo_week(String to_week) {
		this.to_week = to_week;
	}
	public List<String> getTypes() {
		return types;
	}
	public void setTypes(List<String> types) {
		this.types = types;
	}
	public List<String> getStates() {
		return states;
	}
	public void setStates(List<String> states) {
		this.states = states;
	}
	
}
