package com.hometesting.config;


public class CommonContants {
	enum IssueType 
    { 
        BUG, ENHANCEMENT, TASK; 
    } 
	enum IssueState 
    { 
        OPEN, IN_PROGRESS, TESTING, DEPLOY; 
    } 
	
	public static final Integer INITIAL_ISSUE_COUNT = 200;
	public static final int INITIAL_ISSUE_YEAR = 2019;
	
}
