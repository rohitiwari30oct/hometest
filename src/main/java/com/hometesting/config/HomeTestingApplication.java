package com.hometesting.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.hometesting"})
@EnableCaching
public class HomeTestingApplication {

	public static void main(String[] args) {
		SpringApplication.run(HomeTestingApplication.class, args);
	}

}
