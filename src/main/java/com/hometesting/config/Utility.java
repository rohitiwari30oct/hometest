package com.hometesting.config;

import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.ThreadLocalRandom;

public class Utility {
	public static int getRandom(int min, int max) {
		return ThreadLocalRandom.current().nextInt(min, max + 1);
	}

	public static long getRandomWeekOfYear() {
		return (long)getRandom(1, 52);
	}
	public static Date getStartDateOfWeek(String weekYear) {
 		String[] fromWeek = weekYear.split("W");
		int week = Integer.parseInt(fromWeek[1]);
		int year = Integer.parseInt(fromWeek[0]);
		Calendar calendar = Calendar.getInstance();
		calendar.clear();
		calendar.set(Calendar.WEEK_OF_YEAR, week);
		calendar.set(Calendar.YEAR, year);
		return calendar.getTime();
	}
	public static Date getEndDateOfWeek(String weekYear) {
 		String[] toWeek = weekYear.split("W");
		int week = Integer.parseInt(toWeek[1]);
		int year = Integer.parseInt(toWeek[0]);
		Calendar calendar = Calendar.getInstance();
		calendar.clear();
		calendar.set(Calendar.WEEK_OF_YEAR, week);
		calendar.set(Calendar.YEAR, year);
		calendar.add(Calendar.DATE, 7);
		return calendar.getTime();
	}
	public static String getWeekYearFromDate(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		DecimalFormat formatter = new DecimalFormat("00");
		return calendar.get(Calendar.YEAR)+ "W" + formatter.format(calendar.get(Calendar.WEEK_OF_YEAR));
	}
}
