package com.hometesting.config;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.WeekFields;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.UUID;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.hometesting.entity.Issue;
import com.hometesting.entity.IssueState;
import com.hometesting.entity.Project;
import com.hometesting.entity.StateChangeLog;
import com.hometesting.repository.IssueRepository;
import com.hometesting.repository.IssueStateRepository;
import com.hometesting.repository.ProjectRepository;
import com.hometesting.repository.StateChangeLogRepository;

@Component
public class InitializeData implements InitializingBean {

	@Autowired
	ProjectRepository projectRepository;
	@Autowired
	IssueRepository issueRepository;
	@Autowired
	IssueStateRepository issueStateRepository;
	@Autowired
	StateChangeLogRepository stateChangeLogRepository;

	@Override
	public void afterPropertiesSet() throws Exception {
		//cleanup all data
		issueStateRepository.deleteAll();
		issueRepository.deleteAll();
		projectRepository.deleteAll();
		stateChangeLogRepository.deleteAll();
		Project project = initializeProject();
		initializeStates();
		addRandomIssues(project);
	}
	//Adding 200 Issues at application start
	private void addRandomIssues(Project project) {
		int year = CommonContants.INITIAL_ISSUE_YEAR;
		WeekFields weekFields = WeekFields.of(Locale.getDefault());
		System.out.println("");
		System.out.println("Please wait data initializing ...");
		List<Issue> issues = new ArrayList<>();
		for( int i = 1 ; i <= CommonContants.INITIAL_ISSUE_COUNT ; i++) {
		LocalDateTime date = LocalDateTime.now().withYear(year)
		                                .with(weekFields.weekOfYear(), Utility.getRandomWeekOfYear())
		                                .with(weekFields.dayOfWeek(), 7);
		ZonedDateTime zdt = date.atZone(ZoneId.systemDefault());
		Date output = Date.from(zdt.toInstant());
		Issue issue = new Issue();
		issue.setProject(project);
		if(Utility.getRandomWeekOfYear()%2 == 0) {
			issue.setCurrentState(issueStateRepository.findByStateName(CommonContants.IssueState.OPEN.toString()));
			issue.setIssueType(CommonContants.IssueType.BUG.toString());
		}else if(Utility.getRandomWeekOfYear()%3 == 0) {
			issue.setCurrentState(issueStateRepository.findByStateName(CommonContants.IssueState.IN_PROGRESS.toString()));
			issue.setIssueType(CommonContants.IssueType.TASK.toString());
		}else if(Utility.getRandomWeekOfYear()%5 == 0){
			issue.setCurrentState(issueStateRepository.findByStateName(CommonContants.IssueState.TESTING.toString()));
			issue.setIssueType(CommonContants.IssueType.ENHANCEMENT.toString());
		}else {
			issue.setCurrentState(issueStateRepository.findByStateName(CommonContants.IssueState.DEPLOY.toString()));
			issue.setIssueType(CommonContants.IssueType.BUG.toString());
		}
		issue.setCreateDate(output);
		issue.setShortDescription("issue");
		issue.setIssueId(UUID.randomUUID().toString());
		issueRepository.save(issue);
		
		StateChangeLog log = new StateChangeLog();
		log.setId(UUID.randomUUID().toString());
		log.setIssue(issue);
		log.setCreateDate(output);
		log.setState(issue.getCurrentState());
		stateChangeLogRepository.save(log);
		
		}
		
	}

	private Project initializeProject() {
		// add new project
		Project project = new Project();
		project.setProjectId(UUID.randomUUID().toString());
		project.setShortDescription("Project 1");
		projectRepository.save(project);
		return project;

	}

	IssueState state = new IssueState();

	private void initializeStates() {
		List<IssueState> states = new ArrayList<>();
		Arrays.asList(CommonContants.IssueState.values()).forEach(st -> {
			state = issueStateRepository.findByStateName(st.toString());
			if (state == null) {
				state = new IssueState();
				state.setId(UUID.randomUUID().toString());
				state.setStateName(st.toString());
				state.setCreateDate(new Date());
				state.setOrder(1);
				states.add(state);
			}
		});
		issueStateRepository.saveAll(states);
	}

}
