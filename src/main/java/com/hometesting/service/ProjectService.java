package com.hometesting.service;

import com.hometesting.entity.Project;

public interface ProjectService {

	Iterable<Project> findAllProject();

	Project findProjectById(String project_id);

	Project addProject(Project project);

}
