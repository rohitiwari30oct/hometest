package com.hometesting.service;

import com.hometesting.entity.Issue;
import com.hometesting.entity.StateChangeLog;
import com.hometesting.request.WeeklyIssueListRequest;
import com.hometesting.response.ProjectIssue;
import com.hometesting.response.WeeklyIssueListResponse;

public interface IssueService {
	
	Issue addNewIssue(Issue issue);

	Issue updateIssue(Issue issue);

	Issue getIssueById(String issueId);

	Iterable<Issue> getIssuesByProjectId(String projectId);
	
	Iterable<Issue> getAllIssues();

	WeeklyIssueListResponse getIssuesBetweenDates(WeeklyIssueListRequest request);

	Issue findIssueById(String issueId);

	void saveIssuesFromThirdParty(ProjectIssue response);

	Iterable<StateChangeLog> getAllIssueStateChnages();
 	
}
