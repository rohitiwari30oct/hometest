package com.hometesting.repository;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;
import com.hometesting.entity.Project;

@Repository
public interface ProjectRepository extends ElasticsearchRepository<Project, String> {

	Project findByProjectId(String projectId);
}
