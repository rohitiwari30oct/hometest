package com.hometesting.repository;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;
import com.hometesting.entity.StateChangeLog;

@Repository
public interface StateChangeLogRepository extends ElasticsearchRepository<StateChangeLog, String> {

	Iterable<StateChangeLog> findAllByIssue_IssueId(String issueId);
	StateChangeLog findByIssue_IssueIdAndState_stateName(String issueId, String stateName);
}
