package com.hometesting.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.elasticsearch.annotations.Query;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;
import com.hometesting.entity.Issue;

@Repository
public interface IssueRepository extends ElasticsearchRepository<Issue, String> {

	Iterable<Issue> findAllByProject_ProjectId(String projectId);
	
	@Query("{\"bool\" : {\"must\" : {\"range\" : {\"createDate\" : {\"from\" : \"?0\",\"to\" : \"?0\",\"include_lower\" : true,\"include_upper\" : true}}}}}")
	List<Issue> findIssuesBetweenDates(Date from , Date to);
	
	
	@Query("{\"bool\": {\"must\": [{\"match\": {\"issueType\": \"?0\"}}]}}")
	List<Issue> findByIssueTypeCustom(String issueType);

	Issue findByIssueId(String issueId);
}
