package com.hometesting.repository;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;
import com.hometesting.entity.IssueState;

@Repository
public interface IssueStateRepository extends ElasticsearchRepository<IssueState, String> {

	IssueState findByStateName(String stateName);
}
