package com.hometesting.repository;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;
import com.hometesting.entity.IssueType;

@Repository
public interface IssueTypeRepository extends ElasticsearchRepository<IssueType, String> {
	IssueType findByType(String type);
}
