package com.hometesting.response.model;

import java.util.List;

public class StateSummary{
	String state;
	Integer count;
	List<IssueModel> issues;
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public Integer getCount() {
		return count;
	}
	public void setCount(Integer count) {
		this.count = count;
	}
	public List<IssueModel> getIssues() {
		return issues;
	}
	public void setIssues(List<IssueModel> issues) {
		this.issues = issues;
	}
}