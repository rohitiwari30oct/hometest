package com.hometesting.response;

import java.util.ArrayList;
import java.util.List;

public class IssueThirdPartyModel {

	String issue_id;
	String type;
	String current_state;
	List<ChangeLogs> changelogs = new ArrayList<>();

	public String getIssue_id() {
		return issue_id;
	}

	public void setIssue_id(String issue_id) {
		this.issue_id = issue_id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCurrent_state() {
		return current_state;
	}

	public void setCurrent_state(String current_state) {
		this.current_state = current_state;
	}

	public List<ChangeLogs> getChangelogs() {
		return changelogs;
	}

	public void setChangelogs(List<ChangeLogs> changelogs) {
		this.changelogs = changelogs;
	}
	


}
