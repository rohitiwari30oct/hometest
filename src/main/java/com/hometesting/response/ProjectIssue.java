package com.hometesting.response;

import java.util.ArrayList;
import java.util.List;

public class ProjectIssue {
	String project_id;
	List<IssueThirdPartyModel>  issues = new ArrayList<>();
	public String getProject_id() {
		return project_id;
	}
	public void setProject_id(String project_id) {
		this.project_id = project_id;
	}
	public List<IssueThirdPartyModel> getIssues() {
		return issues;
	}
	public void setIssues(List<IssueThirdPartyModel> issues) {
		this.issues = issues;
	}
	
	

}
