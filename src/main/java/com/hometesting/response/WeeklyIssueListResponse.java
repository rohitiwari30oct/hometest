package com.hometesting.response;

import java.util.ArrayList;
import java.util.List;

import com.hometesting.response.model.WeeklySummary;

public class WeeklyIssueListResponse {
	String project_id;
	List<WeeklySummary> weekly_summaries = new ArrayList<>();
	public String getProject_id() {
		return project_id;
	}
	public void setProject_id(String project_id) {
		this.project_id = project_id;
	}
	public List<WeeklySummary> getWeekly_summaries() {
		return weekly_summaries;
	}
	public void setWeekly_summaries(List<WeeklySummary> weekly_summaries) {
		this.weekly_summaries = weekly_summaries;
	}
}


