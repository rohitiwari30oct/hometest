package com.hometesting.response;


public class ChangeLogs {

	private String changed_on;
	private String from_state;
	private String to_state;

	public String getChanged_on() {
		return changed_on;
	}

	public void setChanged_on(String changed_on) {
		this.changed_on = changed_on;
	}

	public String getFrom_state() {
		return from_state;
	}

	public void setFrom_state(String from_state) {
		this.from_state = from_state;
	}

	public String getTo_state() {
		return to_state;
	}

	public void setTo_state(String to_state) {
		this.to_state = to_state;
	}

}
