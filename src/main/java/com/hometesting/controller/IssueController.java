package com.hometesting.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.hometesting.entity.Issue;
import com.hometesting.entity.StateChangeLog;
import com.hometesting.request.WeeklyIssueListRequest;
import com.hometesting.response.ProjectIssue;
import com.hometesting.response.WeeklyIssueListResponse;
import com.hometesting.service.IssueService;
import com.hometesting.service.ProjectService;

@RestController
@RequestMapping("/issue")
public class IssueController {

	@Autowired
	IssueService issueService;
	@Autowired
	ProjectService projectService;
	@Value("${thirdpartyserver.url}") 
    public String thirdPartyUrl;

	@GetMapping("/weeklyreport")
	public WeeklyIssueListResponse getWeeklyReport(@RequestBody WeeklyIssueListRequest request) {
		return issueService.getIssuesBetweenDates(request);
	}
	
	/**
	 * @param requestBody it should contain project_id key value pair
	 * @return 
	 */
	@GetMapping("/fromthirdparty")
	public void getWeeklyReport(@RequestBody Map<String, String> requestBody) {
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		RestTemplate restTemplate = new RestTemplate();
		Map<String, String> body = new HashMap<>();
		body.put("project_id", requestBody.get("project_id"));
		HttpEntity<Map<String, String>> entity = new HttpEntity<>(body, headers);
		ResponseEntity<ProjectIssue> res = restTemplate.exchange(thirdPartyUrl, HttpMethod.POST, entity, ProjectIssue.class);
		issueService.saveIssuesFromThirdParty(res.getBody());
	}
	

	@GetMapping("/all")
	public Iterable<Issue> getAllIssues() {
		return issueService.getAllIssues();
	}
	@GetMapping("/changelogs")
	public Iterable<StateChangeLog> getAllIssueStateChnages() {
		return issueService.getAllIssueStateChnages();
	}

}
