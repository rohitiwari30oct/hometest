package com.hometesting.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hometesting.entity.Project;
import com.hometesting.service.ProjectService;

@RestController
public class ProjectController {
	
	ProjectService projectService;
	
	@Autowired
	public ProjectController(ProjectService projectService) {
		super();
		this.projectService = projectService;
	}

	@GetMapping("project/all")
	public Iterable<Project> getProjectList() {
		return projectService.findAllProject();
	}
}
