# HomeTest Assignment 

# Tech Used : 
	* Spring Boot
	* Elasticsearch
	* Bucket4j for rate limiting
	* Ehcache 

# Prerequisite to run this project

* Download and run elasiticsearh from [here](https://www.elastic.co/downloads/elasticsearch)
* On project run 200 randomly generated issues will be added in a random added project for testing
* issue/fromthirdparty api will consume third party api by passing project_id in request body, this api will take the response and save it in repository accordingly
* project_id can be get using this url : {env}project/all
* In request body fromWeek, toWeek need to keep between 01 to 52, I haven't put input validations for now
* In request body issue types and states array should be within the provided list
* to get the required response please follow example written below : 

# Example : 

# request: 
{
	"project_id" : "89abf021-5bbc-4fc1-81cf-cde91848ee61", 
	"from_week" : "2019W01", 
	"to_week" : "2019W52", 
	"types" : ["TASK", "BUG", "ENHANCEMENT"], //need to keep these within the list that was in requirement
	"states" : ["OPEN", "INPROGRESS", "DEPLOY"] //need to keep these within the list that was in requirement
}

# response :

{
    "project_id": "89abf021-5bbc-4fc1-81cf-cde91848ee61",
    "weekly_summaries": [
        {
            "week": "2019W01",
            "state_summaries": [
                {
                    "state": "DEPLOY",
                    "count": 2,
                    "issues": [
                        {
                            "issue_id": "d1c54e6c-2d70-449e-8dd6-f36868a426ba",
                            "type": "BUG"
                        },
                        {
                            "issue_id": "79f63bd2-8737-4d14-80c9-30d3c2868509",
                            "type": "BUG"
                        }
                    ]
                },
                {
                	.....
                	
                	






